import React from 'react';
import 'antd/dist/antd.css';
import { Table, Tag, Space } from 'antd';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <a>{text}</a>,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: tags => (
      <>
        {tags.map(tag => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'loser') {
            color = 'volcano';
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <Space size="middle">
        <a>Update </a>
        <a>Delete </a>
      </Space>
    ),
  },
];

const data = [
  {
    key: '1',
    name: 'Chandra',
    age: 20,
    address: 'Jogja',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
    name: 'Tito',
    age: 22,
    address: 'Surabaya',
    tags: ['loser','dokter'],
  },
  {
    key: '3',
    name: 'Rizky',
    age: 40,
    address: 'Semarang',
    tags: ['cool', 'teacher'],
  },
];

export default () => <Table columns={columns} dataSource={data} />;