import React from 'react';
import 'antd/dist/antd.css';
import style from "./Progress.module.css";
import { Progress } from 'antd';


export default () => (
  <div className="Progress" style={{ width: "50%", marginLeft:"20px",marginTop:"20px"}}>
    <h1 className={style["Progress-title"]}>Chill
    <Progress percent={30} status="active" />
    </h1>
    <h1 className= {style["Progress-title"]}>Speak
    <Progress percent={50} status="active" />
    </h1>
    <h1 className= {style["Progress-title"]}> Learning
    <Progress percent={70} status="active" />
    </h1>
    <h1 className= {style["Progress-title"]}>Coding
    <Progress percent={80} status="active" />
    </h1>

  </div>
); 