import React from 'react';
import 'antd/dist/antd.css';
import style from "./Breadcrumb.module.css";
import { Breadcrumb } from 'antd';
import { HomeOutlined, UserOutlined } from '@ant-design/icons';

export default () => (
    <div className={style["Breadcrumb"]}>
        <Breadcrumb>
            <Breadcrumb.Item href="">
            <HomeOutlined />
            </Breadcrumb.Item>
            <Breadcrumb.Item href="">
            <UserOutlined />
            <span>Home List</span>
            </Breadcrumb.Item>
            <Breadcrumb.Item>About Me</Breadcrumb.Item>
        </Breadcrumb>
    </div>
);