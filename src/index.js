import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./components/Module";
import Styled from "./Styled";
import Table from "./components/Table";
import Breadcrumb from "./components/Breadcrumb";
import Progress from "./components/Progress";
import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Breadcrumb />
    <Module />
    <h1 className="fw-bold">Table Student</h1>
    <Table/>
    <h1 className="fw-bold">My Progress Bar</h1>
    <Progress />
    <Styled />
  </React.StrictMode>
);
